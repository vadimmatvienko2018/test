$( document ).ready(function() {
    toogleSearch();
    sliderInit();
    showCopyLinkAndLupa();
    tabs();
});
function sliderInit(){
  $('.slider').slick({
      infinite: true,
      dots: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      adaptiveHeight: true,
      prevArrow: $('.slide-left'),
      nextArrow: $('.slide-right')
    });
}
function toogleSearch(){
  $( ".search img" ).click(function() {
  $( ".search input" ).toggle();
});
}
function tabs(){
  $('.tabs').on('click', 'button', function(){
    var tab = $(this).attr('name');
    $('button').removeClass('active');
    $(this).addClass('active');
    $('.projects article').each(function(){
      var project = $(this).data('project');
      if(project == tab){
        $(this).show();
      }else if(tab == 'all'){
        $('article').show();
      }else{
        $(this).css('display', 'none');
      }
    });
  });
}
function showCopyLinkAndLupa(){
  $('.article-wrapper article').find('.copy-link').hide();
  $('.article-wrapper article').find('.lupa').hide();
  $('.article-wrapper article').hover(function(){
    $($(this).find('.copy-link').data('article')).show();
    $($(this).find('.lupa').data('article')).show();
  });
}
function copytext(el) {
    var $tmp = $("<input>");
    $("body").append($tmp);
    $tmp.val($(el).val()).select();
    document.execCommand("copy");
    $tmp.remove();
}
var block_show = false;

function scrollTracking(){
	if (block_show) {
		return false;
	}

	var wt = $(window).scrollTop();
	var wh = $(window).height();
	var et = $('article .count').offset().top;
	var eh = $('article .count').outerHeight();
	var dh = $(document).height();

	if (wt + wh >= et || wh + wt == dh || eh + et < wh){
		block_show = true;
      $('article .count').css('font-size', '14px').animate({fontSize: 48 }, 300);
	}
}

$(window).scroll(function(){
	scrollTracking();
});

$(document).ready(function(){
	scrollTracking();
});
